FROM python:3.8

ARG DEBIAN_FRONTEND=noninteractive
ARG PROJECT=favorite-news
ARG PROJECT_DIR=/${PROJECT}
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR


COPY . .

RUN apt-get update --fix-missing && apt-get upgrade -y && apt-get autoremove && apt-get autoclean

RUN pip install -U --no-cache-dir -r requirements.txt

RUN python -m dostoevsky download fasttext-social-network-model

ENTRYPOINT ["python3"]
CMD ["./parser/main.py"]