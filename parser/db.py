import mysql.connector
from time import sleep
import datetime
import os
from mysql.connector import connect, Error

class DBhelper:
    def __init__(self):
        sleep(10)
        self.conn = connect(
                host=os.getenv("DB_HOST"),
                user= os.getenv("MYSQL_USER"),
                password = os.getenv("MYSQL_PASSWORD"),
                database=os.getenv("MYSQL_DATABASE")
                )
        #инициализация всех таблиц в бд Users, News, Favorites
        self.init_news_dbs()
        self.init_users_dbs()
        self.init_favorites_dbs()

    """
    ИНИЦИАЛИЗАЦИЯ ТАБЛИЦ
    """
    #Инициализация таблицы news    
    def init_news_dbs(self):
        query = """CREATE TABLE IF NOT EXISTS news(
                    id int PRIMARY KEY AUTO_INCREMENT,
                    link VARCHAR(255) NOT NULL,
                    topic VARCHAR(255),
                    title VARCHAR(255),
                    body TEXT,
                    img VARCHAR(255),
                    emotion VARCHAR(255),
                    probability VARCHAR(255)
                ) AUTO_INCREMENT = 1;
                """
        with self.conn.cursor() as cursor:
            cursor.execute(query)

    #Инициализация таблицы users
    def init_users_dbs(self):
        query = """CREATE TABLE IF NOT EXISTS users(
                    id int PRIMARY KEY AUTO_INCREMENT,
                    user_id int NOT NULL,
                    topic_sub VARCHAR(255),
                    emotion_sub VARCHAR(255),
                    date DATE
                ) AUTO_INCREMENT = 1;
                """
        with self.conn.cursor() as cursor:
            cursor.execute(query)

    #Инициализация таблицы favorites
    def init_favorites_dbs(self):
        query = """CREATE TABLE IF NOT EXISTS favorites(
                    fav_id int PRIMARY KEY AUTO_INCREMENT,
                    user_id int NOT NULL,
                    news_id int NOT NULL
                ) AUTO_INCREMENT = 1;
                """
        with self.conn.cursor() as cursor:
            cursor.execute(query)


    """
    МЕТОДЫ ДЛЯ ТАБЛИЦЫ news
    """

    #Вставка новости
    def insert_news_data(self, link, topic, title, body, img, emotion, proba):
        query = f"""INSERT INTO news (link, topic, title, body, img, emotion, probability) 
        VALUES ("{link}", "{topic}", "{title}", "{body}", "{img}", "{emotion}", "{proba}") 
        """
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            self.conn.commit()


    #Выбор последней добавленной новости (возвращается link)
    def get_last_news_link(self):
        query = """SELECT link FROM news ORDER BY ID DESC LIMIT 1"""
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchone()
            if result:
                return result


    """
    МЕТОДЫ ДЛЯ ТАБЛИЦЫ users
    """

    #Добавление новых фильтров по пользователю
    def insert_user_filters(self, user_id, topic_sub, emotion_sub):
        now = datetime.datetime.utcnow()
        query = f"""INSERT INTO users (user_id, topic_sub, emotion_sub, date) 
        VALUES ("{user_id}", "{topic_sub}", "{emotion_sub}", "{now.strftime('%Y-%m-%d')}")
        """
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            self.conn.commit()


    """
    МЕТОДЫ ДЛЯ ТАБЛИЦЫ favorites
    """

    #Добавление новости в избранное
    def insert_news_favorites(self, user_id, news_id):
        query = f"""INSERT INTO favorites (user_id, news_id) 
        VALUES ("{user_id}", "{news_id}")
        """
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            self.conn.commit()
            

    #Удалить новость из избранного
    def delete_news_favorites(self, user_id, news_id):
        query = f"""DELETE * FROM favorites WHERE user_id={user_id} and news_id={news_id}"""
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            self.conn.commit()


    def custom(self, query):
        with self.conn.cursor() as cursor:
            cursor.execute(query)


