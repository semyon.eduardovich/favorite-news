#!/usr/bin/env python
# coding: utf-8

# In[21]:


import requests
from bs4 import BeautifulSoup as BS
import sched, time
import threading
import os
from db import DBhelper
from lxml import etree
from analyzer import getTextEmotion

db = DBhelper()

def main():
    newsLink = get_last_news_link()
    threading.Timer(1200.0, main).start() #перезапуск каждые 300 секунд
    
def test_main():
    #Получаем последнюю новость
    newsLink = get_last_news_link()
    #Проверяем, есть ли эта новость в бд. Если нет, то добавляем (match = True означает, что новости нет)
    match = matching (newsLink)
    #Открываем новость и парсим
    if match == False:
        img, topic, title, split_body = get_full_news(newsLink)
        emotion, proba = getTextEmotion(title)
        #вставка в бд новости вместе с emotion proba
        db.insert_news_data(newsLink, topic, title, split_body, img, emotion, proba)
        
        print(db.get_last_news_link())
        
        
#Собираем полностью информацию о новости: рубрика, заголовок, содержание, картинка
def get_full_news(news_link):
    #Текст с разбивкой по абзацам для загрузки в бд
    split_body = ""
    page = requests.get(news_link)
    soup = BS (page.text, 'html.parser')
    
    topic = soup.select_one("a[class~='topic-header__rubric']").text
    title = soup.select_one("h1[class~='topic-body__titles']").text
    body = soup.select_one("div[class~='topic-body__content']")
    #print("Full topic ", topic)
    #print("Full body ", body)
    #print("Full title ", title)
    #img = 'https://icdn.lenta.ru/images/2023/06/07/21/20230607212340059/owl_detail_240_9f25b829df9dcdcd4e19c39e5a6c8a24.jpg'  
    #img =soup.select_one("img[class~='picture__image']")['src']
    try:
        img= soup.select_one("img[class~='picture__image']")['src']
    except TypeError:
        img = ""

    for p in body:
        split_body+=(p.text)+'\n\n'

    return img, topic, title, split_body


#Функция получения последней новости с ленты
def get_last_news_link():
    page = requests.get("https://lenta.ru/rss")
    soup = BS(page.text, 'xml')
    item = soup.find('item')
    return item.find('link').text

#Функция проверки, есть ли эта новость в БД
def matching(news_link):
    last_news_link = db.get_last_news_link()
    if last_news_link == None:
        return False
    if last_news_link != news_link and news_link != None:
        return False
    return True 
    # CLEAR TABLE
    #db.custom("TRUNCATE TABLE links_news")
        
    #title
    #enclosure url (picture)
    #category
    #link

if __name__ == '__main__':
    # main() (автономочка)
    test_main()
    #DBhelper()




