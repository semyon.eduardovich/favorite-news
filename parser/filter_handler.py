import re

def check(text):
    base = "Россия, Мир, Бывший СССР, Экономика, Наука и техника, Культура, Спорт, Интернет и СМИ, Ценности, Путешествия, Из жизни, Среда обитания, Забота о себе"
    base = base.upper()
    text = text.upper()
    emotion_base = "0123"
    try:
        rows = text.split('\n')
        
        list_of_filters = []
        
        for row in rows:
            topic, emotions = row.split(' - ')
            if topic in base:
                emotions = emotions.replace(' ','').split(",")
                if "0" in emotions:
                    list_of_filters.append((topic, "all"))
                else:
                    for emotion in emotions:
                        if emotion in emotion_base:
                            if emotion == "1":
                                emotion = 'neutral'
                            elif emotion == "2":
                                emotion = 'positive'
                            else:
                                emotion = 'negative'
                            list_of_filters.append((topic,emotion))
                        else:
                            return f"Неверно введена эмоция в теме '{topic}': {emotion}"
            else:
                return f"Темы '{topic}' не существует. Проверьте правильность ввода"
        
    
        return list_of_filters
        
    except Exception:
        return None, "Проверьте формат и попробуйте еще раз"

    