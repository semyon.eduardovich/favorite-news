from dostoevsky.tokenization import RegexTokenizer
from dostoevsky.models import FastTextSocialNetworkModel
import fasttext

fasttext.FastText.eprint = lambda x: None

tokenizer = RegexTokenizer()
model = FastTextSocialNetworkModel(tokenizer=tokenizer)

def getTextEmotion(text):
    results = model.predict([text])[0]
    emotion = list(results.keys())[0]
    proba = results[emotion]
    if emotion == 'speech' or emotion == 'skip':
        emotion = 'neutral'
    return emotion, proba

