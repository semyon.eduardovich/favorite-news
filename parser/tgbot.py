import aiogram
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters import Text
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.dispatcher import FSMContext
import bot_text
import filter_handler

storage = MemoryStorage()
bot = Bot("6158463529:AAEuIH5cZFXd3peYQ14Kno8zACm-FDPwOyo")
dp = Dispatcher(bot, storage=storage)
    
class profileStatesGroup(StatesGroup):
    filters = State()
    fav = State()
    hist = State()
    stat = State()

filter_enter_text = f"""Твои активные фильтры:\n\n"""

#Кнопки
f_filtr = KeyboardButton("Фильтр")
f_fav = KeyboardButton("Избранное")
f_hist = KeyboardButton("История")
f_stat = KeyboardButton("Статистика")
cancel = KeyboardButton("В главное меню")
change_filters = KeyboardButton("Настроить фильтры")


#Главная клавиатура
main_kb = ReplyKeyboardMarkup(resize_keyboard=True).row(f_filtr, f_fav, f_hist, f_stat)

#Клавиатура для фильтров
filter_enter_kb = ReplyKeyboardMarkup(resize_keyboard = True, row_width = 2).row(change_filters, cancel)

#Клавиатуры для отмены
cancel_kb = ReplyKeyboardMarkup(resize_keyboard = True).add(cancel)

@dp.message_handler(commands=['start'], state = None)
async def start_callback(message: types.Message) -> None:
    await message.answer(text = bot_text.hello_text, 
                         parse_mode="Markdown",
                         reply_markup=main_kb)


@dp.message_handler(content_types=['text'], state=None)
async def get_text_messages(message: types.Message) -> None:
    if message.text == "Фильтр":
        
        active_filters = "Тут функция, выводящая активные фильтры пользователя"
        await message.answer(text = filter_enter_text+active_filters, 
                             parse_mode="Markdown",
                             reply_markup=filter_enter_kb)
    if message.text == "Настроить фильтры":
        await message.answer(bot_text.filter_instructions, parse_mode = "Markdown", reply_markup=cancel_kb)
        await profileStatesGroup.filters.set() #Устанавливаем состояние редактирования фильтров
    if message.text == "В главное меню":
        await message.answer("Вернулись к главному меню", reply_markup=main_kb)
    if message.text == "Избранное":
        await message.answer('Список избранных новостей:', reply_markup=cancel_kb)
    if message.text == 'История':
        await message.answer('История изменений фильтров:', reply_markup=main_kb)
    if message.text == 'Статистика':
        await message.answer("Раздел статистики", reply_markup=cancel_kb)
        
@dp.message_handler(content_types=['text'], state = profileStatesGroup.filters)
async def edit_filters(message: types.Message, state = FSMContext) -> None:
    if message.text == "В главное меню":
        await message.answer("Вернулись к главному меню", reply_markup=main_kb)
        await state.finish()
    else:
        async with state.proxy() as data:
            filters = filter_handler.check(message.text)
            if filters[0]:
                #Сюда вписать функцию установки фильтров в БД!
                await message.answer(f"Фильтры установлены! \n {filters}", reply_markup=main_kb)
                await state.finish()
            else:
                await message.answer(filters[1], reply_markup=cancel_kb)
        
# @dp.message_handler(Text(equals="Отмена", ignore_case=True))
# async def cancel(message: type)


       
if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
    
