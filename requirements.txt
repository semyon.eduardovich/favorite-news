requests==2.28.2
lxml==4.9.2
dostoevsky==0.6.0
beautifulsoup4==4.12.0
mysql-connector-python==8.0.33
fasttext==0.9.2
python-telegram-bot==20.3