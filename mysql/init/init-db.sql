CREATE TABLE news(
    id int AUTO_INCREMENT,
    link VARCHAR(255) NOT NULL,
    topic VARCHAR(255),
    title VARCHAR(255),
    body VARCHAR(255),
    img VARCHAR(255),
    emotion VARCHAR(255),
    probability VARCHAR(255)
)

INSERT INTO news (link) VALUES ('hello');